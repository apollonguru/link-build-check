<?php

if(!array_key_exists('urls', $_POST) || !$_POST['urls'])
	return;

$tables = array('sape', 'gogetlinks', 'miralinks');

$inserted_urls = text_to_lines($_POST['urls']);
$inserted_urls_count = count($inserted_urls);

$imploded_urls_for_query = "'".implode("','", $inserted_urls)."'";

$amounts = array();
$query = '';
foreach ($tables as $key => $table) {
	if($key)
		$query .= " UNION ";
	$query .= "SELECT *, '$table' as 'table' FROM $table WHERE url IN (".$imploded_urls_for_query.")";

	$amounts[$table]['amount'] = 0;

	$amounts[$table]['count'] = 0;
}

$db = db::getInstance();
if (!$result = $db->query($query)) {
    printf("Ошибка: %s\n", $db->sqlstate);
    return;
}
?>

<?php if ($result) :
	$total = 0;
	$count = 0;
	$data_for_print_tables = array();
	while ($row = $result->fetch_assoc ()){
		$data_for_print_tables[$row['url']][$row['table']] = $row['price'];
		$amounts[$row['table']]['amount'] += $row['price'];
		$amounts[$row['table']]['count'] ++;
	}
	
	?>
	<div class="table-wrapper mt-5">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>URL</th>
					<?php foreach ($tables as $table) : ?>
						<th><?php echo $table; ?></th>
					<?php endforeach; ?>
				</tr>
			</thead>
			<tbody>
				<?php $url_number = 0; ?>
				<?php foreach ($inserted_urls as $inserted_url) : $url_number++; ?>
					<tr>
						<td><?php echo $url_number; ?></td>
						<td>
							<?php echo $inserted_url; ?>
						</td>
						<?php foreach ($tables as $table) : ?>
							<td>
								<?php echo @$data_for_print_tables[$inserted_url][$table]; ?>
							</td>
						<?php endforeach; ?>
					</tr>
				<?php endforeach; ?>
				<tr>
					<td>Всего:</td>
					<td><?php echo $inserted_urls_count;?> ссылок</td>
					<?php foreach ($tables as $table) : ?>
						<td>
							<?php echo @$amounts[$table]['amount']; ?> рублей
						</td>
					<?php endforeach; ?>
				</tr>
				<tr>
					<td>%</td>
					<td>100%</td>
					<?php foreach ($tables as $table) : ?>
						<td>
							<?php echo round(($amounts[$table]['count'] / $inserted_urls_count) * 100); ?> %
						</td>
					<?php endforeach; ?>
				</tr>
			</tbody>
		</table>
	</div>

	<?php $result->free_result(); ?>

<?php endif; ?>

<?php 

function text_to_lines($text)
{
	if(!$text) return;

	$text = trim($text);

	$lines = explode("\n", $text);

	$lines = array_map('trim', $lines);

	// $lines = array_map('get_domain_name_from_url', $lines);

	$lines = array_filter($lines);

	return $lines;
}

function get_domain_name_from_url($url){
	$parsed_url = parse_url($url);
}

function url_filter($str)
{
	$str = preg_replace('/[a-zA-ZА-Яа-я0-9$-_.+!*(),{}|\\^~\[\]`<>#%"\';\/?:@&=.]/u', '' ,$str);
	return $str;
}
 ?>